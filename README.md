# README

## About
A python script to run MC^2 calculations with an interface for VASP.

## Bash script on HPC
```Bash
module load intelmpi python/2.7
cd $PBS_O_WORKDIR
python mc2.py 300
```