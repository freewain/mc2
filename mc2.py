import os
import shutil
import argparse
import numpy as np
import pdb

BOLTZMANN   = 1.38064852e-23
COULOMB     = 1.60217662e-19
MPI_VASP    = 'mpiexec ~/Apps/VASP/Ruby/VASP_5.4.1/bin/vasp_std'
RES_FILE    = 'Results'
REJ_FILE    = 'Rejected'
STR_DIR     = 'Structs/'
OUT_DIR     = 'Outputs/'

SAVE_CHG    = True
SAVE_POS    = True
CHG_DIR     = 'Charges/'
POS_DIR     = 'Unrelax/'

T_MINIMUM   = 100.0
T_MAXIMUM   = 400.0

ONE_TO_ALL  = 5
flip_flag   = 0


class Poscar(object):
    """Read POSCAR
    header      - first line
    lat_const   - second line
    lat_vects   - 3-5th lines
    elem_names  - 6th line
    elem_count  - 7th line
    coord       - 8th line
    atom_coord  - 9th to end
    Other useful things:
    num_elems   - number of elements
    num_atoms   - number of atoms
    atom_elem   - element of each atom
    """
    def __init__(self, filename):
        "Read from POSCAR."
        with open('POSCAR') as f:
            lines = f.readlines()
        self.num_elems  = len(lines[5].split())
        self.elem_names = lines[5].split()
        self.elem_count = np.zeros(self.num_elems, dtype=int)
        with open(filename) as f:
            lines = f.readlines()
        self.header     = lines[0]
        self.lat_const  = float(lines[1])
        self.lat_vects  = np.loadtxt(lines[2:5])
        elem_names = set(lines[5].split())
        elem_count = lines[6].split()
        matched = [x for x, item in enumerate(self.elem_names) if item in \
            elem_names]
        for i in range(len(elem_names)):
            self.elem_count[matched[i]] = int(elem_count[i])
        self.num_atoms  = np.sum(self.elem_count)
        self.coord      = lines[7]
        self.atom_coord = np.loadtxt(lines[8:9 + self.num_atoms])
        self.atom_elem  = np.zeros(self.num_atoms, dtype=int)
        count = 0
        for i in range(self.num_elems):
            this_elem = self.elem_count[i]
            for j in range(this_elem):
                self.atom_elem[count] = i
                count += 1

    def flip(self):
        "Flip a random atom with probabilities equal to concentrations."
        p = Poscar('POSCAR')
        (r1, r2) = np.random.randint(self.num_atoms, size=2)
        elem1 = self.atom_elem[r1]
        elem2 = p.atom_elem[r2]
        if elem1 == elem2:
            return False
        else:
            self.elem_count[elem1] -= 1
            self.elem_count[elem2] += 1
            self.atom_elem[r1] = elem2
            rearrange = np.argsort(self.atom_elem)
            self.atom_coord = self.atom_coord[rearrange,:]
            return True

    def write_to_file(self, dir):
        "Print POSCAR and POTCAR under given dir."
        if not os.path.exists(dir):
            os.mkdir(dir)
        matched = [x for x, item in enumerate(self.elem_count) if item > 0]
        with open(dir + '/POSCAR', 'w') as f:
            f.write(self.header)
            f.write(str(self.lat_const) + '\n')
            for i in range(3):
                f.write('{:14.8f}{:14.8f}{:14.8f}\n'.format(
                    *self.lat_vects[i,:]))
            f.write(' '.join([str(self.elem_names[x]) for x in matched]) + '\n')
            f.write(' '.join([str(self.elem_count[x]) for x in matched]) + '\n')
            f.write(self.coord)
            for i in range(self.num_atoms):
                f.write('{:14.8f}{:14.8f}{:14.8f}\n'.format(
                    *self.atom_coord[i,:]))
        with open('POTCAR') as f:
            lines = f.readlines()
        line_nums = [0]
        line_nums += [x + 1 for x, item in enumerate(lines) if 'End' in item]
        with open(dir + '/POTCAR', 'w') as f:
            for i in matched:
                for j in range(line_nums[i], line_nums[i + 1]):
                    f.write(lines[j])
        shutil.copy('KPOINTS', dir)
        shutil.copy('INCAR', dir)


class Results(object):
    """ Results for a line in file 'Results'
    num, (num_elems, e0) * num_cell, x_i * num_cell, e0, t*s, e(t)
    defined as:
    step          cell                  ratio        e0   ts   et
    """
    def __init__(self):
        "Read current results."
        with open(RES_FILE) as f:
            lines = f.readlines()
        if len(lines) == 1:
            self.read_tmp1()
        else:
            self.read_results()

    def read_tmp1(self):
        "Read results only from TMP-1."
        p = Poscar('TMP-1/POSCAR')
        n = p.num_elems
        self.step = 1
        self.cell = np.zeros((n, n + 1))
        with open('TMP-1/OSZICAR') as f:
            e0 = float(f.readlines()[-1].split()[4])
        for i in range(n):
            self.cell[i, :n] = p.elem_count[:]
            self.cell[i, n] = e0
        self.ratio = np.array([1.0 / n] * n)
        self.e0 = e0
        self.ts = -t_entro * BOLTZMANN / COULOMB * np.sum([x * np.log(
            float(x) / p.num_atoms) for x in p.elem_count])
        self.et = self.e0 - self.ts
        self.p  = 1.0
        self.t_sa = sa_entropy(self.ts)

    def read_results(self):
        "Read results from 'Results' file."
        with open(RES_FILE) as f:
            last = f.readlines()[-1].split()
        self.step = int(last[0])
        n = int(np.sqrt(len(last) - 3)) - 1
        self.cell = np.zeros((n, n + 1))
        for i in range(n):
            for j in range(n + 1):
                k = i * (n + 1) + j + 1
                self.cell[i, j] = float(last[k])
        self.ratio = np.array([float(x) / 100 for x in last[-(n + 5):-5]])
        self.e0 = float(last[-5])
        self.ts = float(last[-4])
        self.et = float(last[-3])
        self.p  = float(last[-2])
        self.t_sa = float(last[-1])

    def read_tmp(self, dirs):
        "Read from TMP directories."
        p = Poscar('POSCAR')
        n = p.num_elems
        for i in dirs:
            ptmp = Poscar(i + '/POSCAR')
            k = int(i.split('-')[1]) - 1
            self.cell[k, :n] = ptmp.elem_count[:]
            if os.path.exists(i + '/OSZICAR'):
                with open(i + '/OSZICAR') as f:
                    last_line = f.readlines()[-1].split()
                self.cell[k, n] = float(last_line[4])
        with open(RES_FILE) as f:
            self.step = int(f.readlines()[-1].split()[0]) + 1
        arr = np.array([p.elem_count])
        arr = np.concatenate((self.cell[:, :n].T, arr.T), axis=1)
        self.ratio = calc_ratio(arr)
        self.e0 = np.sum([self.cell[x][n] * self.ratio[x]
            for x in range(n)])
        self.ts = 0.0
        for i in range(n):
            self.ts += self.ratio[i] * (-t_entro * BOLTZMANN / COULOMB *
                np.sum([x * np.log(x / p.num_atoms)
                    for x in self.cell[i, :n] if x > 0]))
        self.et = self.e0 - self.ts
        self.t_sa = sa_entropy(self.ts)

    def add_results(self, filename):
        "Add current results to 'Results' file."
        n = len(self.cell)
        with open(filename, 'a+') as f:
            f.write(str(self.step).zfill(digit))
            for i in range(n):
                for j in range(n):
                    f.write('{:3.0f}'.format(self.cell[i, j]))
                f.write('{:10.3f}'.format(self.cell[i, n]))
            for i in range(n):
                f.write('{:7.2f}'.format(self.ratio[i] * 100))
            f.write('{:10.3f}{:7.3f}{:10.3f}{:6.2f}{:8.1f}\n'.format(self.e0,
                self.ts, self.et, self.p, self.t_sa))


def prep():
    "Make sure all inputs exist and prepare all needed directories."
    if os.path.exists('stop'):
        os.remove('stop')
    files = ['INCAR', 'KPOINTS', 'POTCAR', 'POSCAR']
    for i in files:
        if not os.path.exists(i):
            print i + ' not found!'
            return False
    p = Poscar('POSCAR')
    for i in range(p.num_elems):
        s = 'TMP-' + str(i + 1)
        if not os.path.exists(s):
            os.mkdir(s)
    if not os.path.exists(OUT_DIR):
        os.mkdir(OUT_DIR)
    if not os.path.exists(STR_DIR):
        os.mkdir(STR_DIR)
    if SAVE_POS and not os.path.exists(POS_DIR):
        os.mkdir(POS_DIR)
    if SAVE_CHG and not os.path.exists(CHG_DIR):
        os.mkdir(CHG_DIR)
    return True


def run_vasp(tmp):
    "Run VASP under given TMP directory."
    if not os.path.exists(tmp):
        print tmp + 'does not exist for running VASP.'
        return False
    cwd = os.getcwd()
    os.chdir(cwd + '/' + tmp)
    os.system(MPI_VASP + ' > vasp.out')
    os.chdir(cwd)
    if not os.path.exists(tmp + '/OSZICAR'):
        return False
    with open(tmp + '/OSZICAR') as f:
        last = f.readlines()[-1]
    if last.split()[1] == 'F=':
        return True
    else:
        return False


def clean_tmp(num_cell):
    "Delete all files under all TMP directories."
    for i in range(num_cell):
        folder = 'TMP-' + str(i + 1)
        for the_file in os.listdir(folder):
            file_path = os.path.join(folder, the_file)
            if os.path.isfile(file_path):
                os.unlink(file_path)


def calc_ratio(arr):
    "Calculate the ratios from (n+1)*n input."
    n = len(arr)
    A = arr[:,:n]
    B = arr[:,n]
    X, res, rank, s = np.linalg.lstsq(A, B)
    C = np.dot(A, X)
    if all(-1e-8 <= x <= 1 + 1e-8 for x in X) and all(abs(x) < 1e-6 for x in B-C):
        return abs(X)
    else:
        return np.zeros(n)


def sa_entropy(ts):
    "Use entropy to calculate the current temperature for simulated annealing."
    t_max = np.maximum(t_entro * 1.3, T_MINIMUM)
    t_max = np.minimum(t_max, T_MAXIMUM)
    t_min = 0.0
    with open(RES_FILE, 'a+') as f:
        lines = f.readlines()
    if len(lines) < 2:
        return t_max
    else:
        ts_init = float(lines[1].split()[-4])
        return t_min + (t_max - t_min) * ts / ts_init


def get_neighbor(name):
    "Neighbor function to get next-step structures."
    global flip_flag
    p = Poscar('POSCAR')
    n = p.num_elems
    clean_tmp(n)
    if name == 'flip_all':
        r = Results()
        s = []
        for i in range(n):
            ptmp = Poscar(STR_DIR + 'POSCAR_{}_{}'.format(
                str(r.step).zfill(digit), i + 1))
            if ptmp.flip():
                s.append('TMP-{}'.format(i + 1))
                ptmp.write_to_file('TMP-{}'.format(i + 1))
        flip_flag = 0
        if len(s) != 0:
            r.read_tmp(s)
            if np.sum(r.ratio) == 0:
                s = []
        return s
    elif name == 'flip_one':
        r = Results()
        s = []
        rand = np.random.rand()
        sum = 0.0
        for i in range(n):
            sum += r.ratio[i]
            if rand <= sum:
                ptmp = Poscar(STR_DIR + 'POSCAR_{}_{}'.format(
                    str(r.step).zfill(digit), i + 1))
                if ptmp.flip():
                    s = ['TMP-{}'.format(i + 1)]
                    ptmp.write_to_file('TMP-{}'.format(i + 1))
                break
        flip_flag += 1
        if len(s) != 0:
            r.read_tmp(s)
            if np.sum(r.ratio) == 0:
                s = []
        return s
    elif name == 'flip_alt':
        if flip_flag == ONE_TO_ALL:
            return get_neighbor('flip_all')
        else:
            return get_neighbor('flip_one')
    else:
        print "Something is wrong with get_neighbor() function."
        return []


def new_job():
    "Start a new calculation."
    p = Poscar('POSCAR')
    n = p.num_elems
    clean_tmp(n)
    s = '{:>{}}'.format('No.', digit)
    for i in range(n):
        for j in range(n):
            s += '{:>3}'.format(p.elem_names[j])
        s += '{:>9}{}'.format('E_0^', i + 1)
    for i in range(n):
        s += '{:>7}'.format('x_' + str(i + 1) + '%')
    s += '{:>10}{:>7}{:>10}{:>6}{:>8}\n'.format('E_tot', 'T*S',
        'E(' + str(t_entro) + 'K)', 'P', 'T_sa')
    with open(RES_FILE, 'w') as f:
        f.write(s)
    with open(REJ_FILE, 'w') as f:
        f.write(s)
    p.write_to_file('TMP-1')
    if not run_vasp('TMP-1'):
        print "VASP under TMP-1 failed."
        exit()
    shutil.copy('TMP-1/vasp.out', OUT_DIR +
        'vasp_{}_1'.format(str(1).zfill(digit)))
    shutil.copy('TMP-1/POSCAR', POS_DIR +
        'POSCAR_{}_1'.format(str(1).zfill(digit)))
    shutil.copy('TMP-1/CHGCAR', CHG_DIR +
        'CHGCAR_{}_1'.format(str(1).zfill(digit)))
    r = Results()
    r.add_results(RES_FILE)
    for i in range(n):
        shutil.copy('TMP-1/CONTCAR', STR_DIR + 'POSCAR_{}_{}'.format(
            str(1).zfill(digit), i + 1))


def main():
    "Main function to control the algorithm."
    global t_entro, anneal, digit, neighbor
    parser = argparse.ArgumentParser()
    parser.add_argument('T', type=int,
            help='integer temperature for entropy calculation')
    parser.add_argument('-a', '--anneal', nargs=1, default='entropy',
            help='simulated annealing: entropy (default)')
    parser.add_argument('-d', '--digit', nargs=1, type=int, default='4',
            help='maximum digits of steps (default: 4)')
    parser.add_argument('-n', '--neighbor', nargs=1, default='flip_alt',
            help='neighbor function: flip_alt (default), flip_all, flip_one')
    args = parser.parse_args()
    t_entro = args.T
    anneal  = args.anneal
    digit   = args.digit
    neighbor= args.neighbor
    if not prep():
        exit()
    with open(RES_FILE, 'a+') as f:
        lines = f.readlines()
        if len(lines) < 2:
            new_job()
    while True:
        if os.path.exists('stop'):
            os.remove('stop')
            break
        with open(RES_FILE, 'a+') as f:
            lines = f.readlines()
            if len(lines) >= np.power(10, digit):
                break
        dirs = get_neighbor(neighbor)
        if len(dirs) == 0:
            continue
        vasp_good = True
        for i in dirs:
            if not run_vasp(i):
                print "VASP under " + i + " failed."
                vasp_good = False
                break
        if not vasp_good:
            continue
        r1 = Results()
        r2 = Results()
        r2.read_tmp(dirs)
        prob = np.exp((r1.et - r2.et) * COULOMB / BOLTZMANN / r2.t_sa)
        r2.p = np.minimum(prob, 1.0)
        if np.random.rand() < r2.p:
            r2.add_results(RES_FILE)
            for i in range(len(r1.cell)):
                src = 'POSCAR_{}_{}'.format(str(r1.step).zfill(digit), i + 1)
                dst = 'POSCAR_{}_{}'.format(str(r2.step).zfill(digit), i + 1)
                shutil.copy(STR_DIR + src, STR_DIR + dst)
            for i in dirs:
                x = i.split('-')[1]
                src = '{}/CONTCAR'.format(i)
                dst = 'POSCAR_{}_{}'.format(str(r2.step).zfill(digit), x)
                shutil.copy(src, STR_DIR + dst)
                src = '{}/vasp.out'.format(i)
                dst = 'vasp_{}_{}'.format(str(r2.step).zfill(digit), x)
                shutil.copy(src, OUT_DIR + dst)
                if SAVE_CHG:
                    src = '{}/CHGCAR'.format(i)
                    dst = 'CHGCAR_{}_{}'.format(str(r2.step).zfill(digit), x)
                    shutil.copy(src, CHG_DIR + dst)
                if SAVE_POS:
                    src = '{}/POSCAR'.format(i)
                    dst = 'POSCAR_{}_{}'.format(str(r2.step).zfill(digit), x)
                    shutil.copy(src, POS_DIR + dst)
        else:
            r2.add_results(REJ_FILE)


if __name__ == '__main__':
    main()